package adwin.Maven_demo1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Basics {
    private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static WebDriver setChromeDriver(WebDriver driver) {
		if (isWindows()) {
			System.setProperty("webdriver.chrome.driver", "driver/windows/chromedriver.exe");
		} else if (isMac()) {
			System.setProperty("webdriver.chrome.driver", "driver/mac/chromedriver");
		} else {
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors", "--remote-debugging-port=9222", "--no-sandbox");

		driver = new ChromeDriver(options);
		return driver;
	}

	public static WebDriver setFireFoxDriver(WebDriver driver) {
		if (isWindows()) {
			System.setProperty("webdriver.gecko.driver", "driver/windows/geckodriver.exe");
		} else if (isMac()) {
			System.setProperty("webdriver.gecko.driver", "driver/mac/geckodriver");
		} else {
			System.setProperty("webdriver.gecko.driver", "driver/linux/geckodriver");
		}
		FirefoxOptions options = new FirefoxOptions();
		options.setHeadless(true);
		driver = new FirefoxDriver(options);
		return driver;
	}

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isUnix() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
		
	}

	public static boolean isSolaris() {

		return (OS.indexOf("sunos") >= 0);
	}

}