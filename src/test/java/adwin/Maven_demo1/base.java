package adwin.Maven_demo1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class base {

     @BeforeSuite
     public void testBeforeSuite() {
         System.out.println("Suite 1 Start");
     }

     @AfterSuite
     public void testAfterSuite() {
         System.out.println("Suite 1 End");
     }
}