package adwin.Maven_demo1;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PersonalRegoTest {
    static WebDriver driver;

    @BeforeClass
	public void login() {
        driver = Basics.setChromeDriver(driver);
        System.out.println("Before Class");
    }
    
    @BeforeTest
    public void beforeTest() {
        System.out.println("New Test");
        System.out.println();
    }

	@Test(alwaysRun = false)
	public void bad() throws InterruptedException {
        System.out.println("Step 1");
    }
    
    @Test(dependsOnMethods = "bad", alwaysRun = true)    
    public void good() {
        System.out.println("Step 2");
    } 

    @Test(dependsOnMethods = "good", alwaysRun = true)
    public void neutral() {
        System.out.println("Step 3");
    }

	@AfterClass
	public void exit() {
        driver.quit();
        System.out.println("End");

	}
}  