package adwin.Maven_demo1;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class SampleFactory {

    @DataProvider(name = "credentials")
    public static Object[][] dataProvider() {        
        return new Object[][] {
            {"Singapore", "test1@testmail.com", "google.com"}, 
            {"USA", "test2@testmail.com", "facebook.com"}
        };
    }

//    @Factory(dataProvider = "credentials")
//    public Object[] CreateInstances(String country, String email, String instance) {
//        return new Object[] {new PersonalRegoTest(country, email, instance)};
//
//    }

}
